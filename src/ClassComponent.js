import React, { Component } from 'react';
import './App.css';

export class ClassComponent extends Component {
  render() {
    return (
      <div className='App'>
        <h1>Class Component</h1>
        <p>This from class Component</p>
      </div>
      
    )
  }
}

export default ClassComponent